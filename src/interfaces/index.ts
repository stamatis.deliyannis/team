export interface IUser {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
}
export interface IRouteInfo {
  id: string;
}

export interface INewUser {
  _id: null;
  firstName: string;
  lastName: string;
  email: string;
}
