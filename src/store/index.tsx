import React from 'react';

//Creating Store
export const Store = React.createContext({});

const initialState = {
  users: [],
  message: ''
};

export function reducer(state = initialState, action: any) {
  switch (action.type) {
    case 'FETCH_DATA':
      return {
        ...state,
        users: action.payload.data
      };
    case 'ADD_USER':
      return {
        ...state,
        users: [...state.users, action.payload]
      };
    case 'EDIT_USER':
      return {
        ...state,
        users: action.payload
      };
    case 'DELETE_USER':
      return {
        ...state,
        users: action.payload
      };
    case 'MESSAGE':
      return {
        ...state,
        message: action.payload
      };
    default:
      return state;
  }
}

export function StoreProvider(props: any) {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const value = { state, dispatch };
  console.log(props);
  return <Store.Provider value={value}>{props.children}</Store.Provider>;
}
