import React, { useContext, useEffect } from 'react';
import { IUser } from '../interfaces';
import User from './User';
import { Store } from '../store';
import { fetchDataAction } from '../actions';
import styled from 'styled-components';
import { Info } from '../css/styledComponents';

const UsersContainer = styled.div`
  border: 1px solid whitesmoke;
  padding: 1rem;
  @media (max-width: 500px) {
    border: none;
    padding: 0;
  }
`;

export default function Users(props: any): JSX.Element {
  const { state, dispatch }: any = useContext(Store);
  const { users, message } = state;

  useEffect(() => {
    // this check will stop fetching when navigating
    users.length === 0 && fetchDataAction(dispatch);
  }, [users.length, dispatch]);

  const revUsers = users.slice().reverse();

  return (
    <main>
      <Info>
        <h4>Click on a user to edit</h4>
        {''}
      </Info>
      <p>{message ? `Status: ${message}` : ''}</p>
      <UsersContainer>
        {revUsers.length ? (
          revUsers.map((user: IUser) => <User user={user} key={user._id} />)
        ) : (
          <img
            src={require('../loading.svg')}
            style={{ maxWidth: '15rem' }}
            alt="spinner"
          />
        )}
        {revUsers.length === 0 ? 'No users received yet' : ''}
      </UsersContainer>
    </main>
  );
}
