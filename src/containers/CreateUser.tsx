import React, { useState } from 'react';
import { IUser } from '../interfaces';
import { createUserAction } from '../actions';
import { Store } from '../store';
import { Button, StyledForm, Info } from '../css/styledComponents';

export default function CreateUsers(props: any) {
  const { dispatch }: any = React.useContext(Store);

  const initialFormState: IUser = {
    _id: '',
    firstName: '',
    lastName: '',
    email: ''
  };

  const [user, setUser] = useState(initialFormState);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    createUserAction(dispatch, user);
    props.history.push('/users');
  };

  const handleInputChange = (
    event: React.FormEvent<HTMLInputElement>
  ): void => {
    const { name, value } = event.currentTarget;
    setUser({ ...user, [name]: value });
  };

  return (
    <>
      {user && (
        <StyledForm onSubmit={handleSubmit}>
          <Info>
            <h4>
              Create New User
              <hr />
            </h4>
          </Info>
          <fieldset>
            <legend>Please fill the required information</legend>

            <div>
              <label htmlFor="firstName">First Name</label>
              <input
                type="text"
                name="firstName"
                placeholder="First name, eg.: Adam"
                onChange={handleInputChange}
                required
              />
            </div>

            <div>
              <label htmlFor="lastName">Last Name</label>
              <input
                type="text"
                name="lastName"
                placeholder="Last name, eg.: Smith"
                onChange={handleInputChange}
                required
              />
            </div>

            <div>
              <label htmlFor="email">Email</label>
              <input
                type="email"
                name="email"
                placeholder="example@gmail.com"
                onChange={handleInputChange}
                required
              />
            </div>
            <Button type="submit">Create User</Button>
          </fieldset>
        </StyledForm>
      )}
    </>
  );
}
