import React, { useState, useEffect } from 'react';
import { Store } from '../store';
import { IUser, IRouteInfo } from '../interfaces';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { editUserAction, deleteUserAction } from '../actions';
import { Button, StyledForm, ButtonContainer } from '../css/styledComponents';
import { messageAction } from '../actions';
import styled from 'styled-components';

const ButtonDanger = styled(Button)`
  background-color: #d46868;
  color: #3d1010;
  &:hover {
    background-color: #6b3636;
    color: #555;
  }
`;

function EditUser({ location, history }: RouteComponentProps<IRouteInfo>) {
  const { state, dispatch }: any = React.useContext(Store);
  const { users, message } = state;

  //resetting messages
  useEffect(() => {
    messageAction(dispatch, '');
  }, [dispatch]);

  // Find user to edit based on router location
  const user: IUser = location.state.user;

  const [updatedUser, setUser] = useState(user);

  const handleInputChange = (
    event: React.FormEvent<HTMLInputElement>
  ): void => {
    const { name, value } = event.currentTarget;
    setUser({ ...updatedUser, [name]: value });
  };

  const handleDeleteUser = () => {
    deleteUserAction(dispatch, user._id, users);
    history.push('/users');
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    await editUserAction(dispatch, updatedUser, users, user._id);
  };

  return (
    <>
      <StyledForm onSubmit={handleSubmit}>
        <h4>
          Edit User: {user.firstName} {user.lastName}
          <hr />
        </h4>
        <fieldset>
          <legend>Please fill the required information</legend>

          <div>
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              name="firstName"
              placeholder="First name, eg.: Adam"
              defaultValue={user.firstName}
              onChange={handleInputChange}
              required
            />
          </div>

          <div>
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              name="lastName"
              placeholder="Last name, eg.: Smith"
              defaultValue={user.lastName}
              onChange={handleInputChange}
              required
            />
          </div>

          <div>
            <label htmlFor="email">Email</label>
            <input
              type="email"
              name="email"
              placeholder="example@gmail.com"
              defaultValue={user.email}
              onChange={handleInputChange}
              required
            />
          </div>
          <ButtonContainer>
            <Button type="submit">Edit User</Button>
            <ButtonDanger
              type="button"
              onClick={handleDeleteUser}
              className="btn"
            >
              Delete User
            </ButtonDanger>
          </ButtonContainer>
        </fieldset>
        <p>{message ? `Status: ${message}` : ''}</p>
      </StyledForm>
    </>
  );
}
export default withRouter(EditUser);
