import React from 'react';
import Users from './Users';
import CreateUser from './CreateUser';
import EditUser from './EditUser';
import ErrorPage from './ErrorPage';
import { Route, Switch, Link, Redirect } from 'react-router-dom';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 3rem 15rem;
  @media (max-width: 1000px) {
    padding: 3rem 10rem;
  }
  @media (max-width: 700px) {
    padding: 3rem 5rem;
  }
  @media (max-width: 500px) {
    padding: 0rem 0rem;
  }
`;

const StyledHeader = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 5rem;
  background-color: #1a212b;
  padding: 1rem;
  > a,
  h1 {
    color: #ffbb00;
  }
`;
const CreateButton = styled.button`
  width: 70px;
  height: 70px;
  border-radius: 50%;
  border: 2px solid #453200;
  background-color: green;
  display: flex;
  align-items: center;
  position: fixed;
  bottom: 2rem;
  right: 2rem;
  text-align: center;
  background-color: #ffbb00;
  color: #070404;
  cursor: pointer;
  font: 16px sans-serif;
  @media (max-width: 360px) {
    bottom: 1rem;
    right: 1rem;
  }
`;

const UsersButton = styled.div`
  border: 1px solid #ffbb00;
  padding= 0.5rem;
  border-radius: 10px;
  height: 40px;
  display: flex;
  align-items: center;
  text-align:center;
`;

export default function App(props: any): JSX.Element {
  return (
    <Wrapper>
      <header>
        <StyledHeader>
          <Link to="/users">
            <h1>Team</h1>
          </Link>
          {/* {The router is used in index} */}
          <nav>
            <UsersButton>
              <Link to="/users" style={{ color: '#ffbb00' }}>
                Users
              </Link>
            </UsersButton>
          </nav>
        </StyledHeader>
      </header>

      <Switch>
        <Route exact path="/" component={Users} />
        <Route exact path="/users" component={Users} />
        <Route exact path="/users/edit/:id" component={EditUser} />
        <Route path="/users/create" component={CreateUser} />
        {/* Maybe keep the path so the user sees
              the bad url. Possible implementation:
              <Route path='*' exact={true} component={ErrorPage} />
            */}
        <Route path="/error" component={ErrorPage} />
        <Redirect from="*" to="/error" />
      </Switch>
      <Link to="/users/create">
        <CreateButton>Create User</CreateButton>
      </Link>
      {props.children}
    </Wrapper>
  );
}
