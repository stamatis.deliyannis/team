import React from 'react';
import styled from 'styled-components';

const ErroPage = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  padding: 2rem;
`;

export default function ErrorPage() {
  return (
    <ErroPage>
      <h3>Something went wrong.</h3>
      <p>Try clicking on a link.</p>
    </ErroPage>
  );
}
