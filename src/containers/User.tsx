import React, { useState } from 'react';
import { IUser } from '../interfaces';
import { Link } from 'react-router-dom';
import { Store } from '../store';
import { deleteUserAction } from '../actions';
import styled from 'styled-components';

const SmallButton = styled.button`
  min-width: 42px;
  min-height: 42px;
  max-width: 42px;
  max-height: 42px;
  border-radius: 50%;
  border: 1px solid #262626;
  background-color: green;
  align-items: center;
  text-align: center;
  background-color: #6e644b;
  color: #9e906d;
  cursor: pointer;
  font: 18px sans-serif;
`;

const ListStyled = styled.div`
  display: flex;
  align-items: center;
  background-color: #333333;
  padding: 0.5rem;
  margin: 0.5rem;
  list-style-type: none;
  align-content: stretch;
  height: 3rem;
  > a {
    width: 100%;
    height: 3rem;
    display: flex;
    justify-content: left;
    align-items: center;
  }
`;

export default function User(props: { user: IUser }) {
  const { dispatch, state }: any = React.useContext(Store);
  const { user } = props;
  const { users } = state;

  // used for storing the right user currently being deleted
  const [deathRow, setDeatRow] = useState(null);

  const handleDelete = (userId: any) => {
    setDeatRow(userId);
    deleteUserAction(dispatch, user._id, users);
  };

  const location = {
    pathname: `/users/edit/${user._id}`,
    state: { user: user }
  };

  return (
    <>
      <ListStyled>
        <SmallButton onClick={() => handleDelete(user._id)}>✖</SmallButton>
        <Link to={location}>
          <div>
            {user._id === deathRow
              ? 'Deleting User...'
              : `${user.firstName} ${user.lastName}, email: ${user.email}`}
          </div>
        </Link>
      </ListStyled>
    </>
  );
}
