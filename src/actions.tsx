import { IUser } from './interfaces';
import axios from 'axios';

const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';
const URL = 'https://api-users.fakiolinho.now.sh/api/users';

export const messageAction = (dispatch: any, text: string) => {
  return dispatch({
    type: 'MESSAGE',
    payload: text
  });
};

export const fetchDataAction = async (dispatch: any) => {
  return axios
    .get(PROXY_URL + URL)
    .then(response => {
      dispatch({
        type: 'FETCH_DATA',
        payload: response.data
      });
    })
    .catch(error => {
      console.log('Error: ', error);
    });
};

export const createUserAction = (dispatch: any, newUser: IUser) => {
  return axios
    .post(PROXY_URL + URL, newUser)
    .then(messageAction(dispatch, 'Creating new User...'))
    .then(response => {
      if (response.status === 200) {
        messageAction(dispatch, 'User successfully created');
        console.log('CREATED', response);
      }
    })
    .then(() =>
      dispatch({
        type: 'ADD_USER',
        payload: newUser
      })
    )
    .then(() => fetchDataAction(dispatch))
    .catch(error => {
      console.log('Error: ', error);
    });
};

export const editUserAction = (
  dispatch: any,
  updatedUser: IUser,
  users: IUser[],
  userId: string
) => {
  return axios
    .put(`${PROXY_URL}${URL}/${userId}`, updatedUser)
    .then(messageAction(dispatch, 'Sending to db...'))
    .then(response => {
      if (response.status === 200) {
        messageAction(dispatch, 'User Successfully Updated');
        console.log('UPDATED', response);
      }
    })
    .then(() => {
      // if you find the user.id, replace the user with the updated version,
      // otherwise keep the old user
      const newVersionList = users.map(user =>
        user._id === updatedUser._id ? updatedUser : user
      );

      dispatch({
        type: 'EDIT_USER',
        payload: newVersionList
      });
    })
    .catch(error => {
      console.log('Error: ', error);
    });
};

export const deleteUserAction = (
  dispatch: any,
  userId: string,
  users: IUser[]
) => {
  // Need to update UI
  // getting list without the deleted user
  const listWithout = users.filter(user => user._id !== userId);

  return (
    axios
      .delete(`${PROXY_URL}${URL}/${userId}`)
      .then(messageAction(dispatch, 'Deleting user...'))
      .then(response => {
        if (response.status === 200) {
          messageAction(dispatch, 'User Deleted');
          console.log('DELETED', response);
        }
      })
      // This will update UI
      .then(() =>
        dispatch({
          type: 'DELETE_USER',
          payload: listWithout
        })
      )
      .catch(error => {
        console.log('Error: ', error);
      })
  );
};
