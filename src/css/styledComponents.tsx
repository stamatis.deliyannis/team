import styled from 'styled-components';
// Only reused components here
export const Button = styled.button`
  display: inline-block;
  *display: inline;
  zoom: 1;
  padding: 6px 20px;
  margin: 0;
  cursor: pointer;
  border: 1px solid #bbb;
  overflow: visible;
  font: bold 13px arial, helvetica, sans-serif;
  text-decoration: none;
  white-space: nowrap;
  color: #555;
  background-color: #ddd;
  background-clip: padding-box;
  border-radius: 3px;
  &::-moz-focus-inner {
    border: 0;
    padding: 0;
  }
  &:hover {
    background-color: #eee;
    color: #555;
  }
  &:active {
    background: #e9e9e9;
    position: relative;
    top: 1px;
    text-shadow: none;
  }
  margin-top: 0.5rem;
`;

export const StyledForm = styled.form`
  max-width: 450px;
  margin: 0 auto;
  border-bottom: 1px solid #ccc;
  > fieldset > div {
    opacity: 0.8;
    position: relative;
    > label {
      opacity: 0.3;
      font-weight: bold;
    }
    > input {
      width: 100%;
      border: 0;
      padding: 10px 10px 10px 10px;
      margin-bottom: 10px;
      background: #f0ecec;
    }
    > fieldset > div:last-child {
      display: flex;
      flex-wrap: nowrap;
    }
  }
`;
export const ButtonContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
`;

export const Info = styled.div`
  @media (max-width: 500px) {
    padding-left: 1rem;
  }
`;
