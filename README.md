# TEAM

---

This is my version of Team app.

## Features

- The user is presented with a list of users collected by the dabase.
- The user can create, edit and delete any user.
- The user is presented with meaningful messages as they commit their actions.

## Technologies

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
TypeScript,
[![style: styled-components](https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg?colorB=daa357&colorA=db748e)](https://github.com/styled-components/styled-components)

## Manual Testing

1.  'homepage' is set to Users:
    a. When the app starts, the path is "/" and Users list displayed.
    b. When the 'Users' link is clicked, we get redirected to "/users" that displays a list of users.

2.  The list items display the correct data for each user.

    - A spinner and a message is displayed while waiting for the list data.

    a. Any list item can be deleted by clicking on the round 'x' button.

    - We get the messages: 'Deleting user', then 'User deleted' (if server response is OK (200))

    b. Upon delete, both the database and the list in the UI.

3.  Each user list item is selectable by clicking on it, which redirects to the '/users/edit' page.

4.  In Edit user page:

    a. The input field are pre-populated with the correct current data.

    b. The user data is editable by typing in the input fields.

    c. Clicking on 'Edit User' button updates the correct info in the database.

    - We get the messages: 'Updating user', then 'User updated' (if server response is OK (200))

    - When navigating to Edit User page, the message reset, so that we don't see confusing info. Makes no sense to see a previous action message while editing.

    d. The user can be deleted by clicking 'Delete User'

    - We get the messages: 'Deleting user', then 'User deleted' (if server response is OK (200))

    - That removes the user from the database, and we get redirected to the Users list.

5.  There is a 'Create User' button. CLicking it redirects to '/users/create' page.

    a. There are 3 input fields: first name, last name, email.
    b. Clicking on 'Create User' stores the user in the database, and we are redirected to Users list page ('/users').

    - We get the messages: 'Creating user', then 'User created' (if server response is OK (200))

6.  If a bad path is typed, we get redirected to Error Page ('/error').

7.  The app is responsive for laptop screens, and mobile phones.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

# Team

Team UI

> > > > > > > 799c5864b2b469fc21dc16fd1ba6bc7694b13d22
